/**
 * Print command-line parameters.
 */
public class Main {
	
	/**
	 * Entry point.
	 * @param args command-line parameters.
	 */
	public static void main(String[] args) {
		for(String s: args) {
			System.out.println(s);
		}
	}
}
