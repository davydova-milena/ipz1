﻿package ua.khpi.project.task07;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Коллекция элементов, наблюдаемый объект.
 * @author davydova_milena
 *
 */
public class ItemCollection extends Observable implements Iterable<Item> {
	
	public static final String ITEMS_CHANGED = "ITEMS_CHANGED";
	
	public static final String ITEMS_EMPTY = "ITEMS_EMPTY";
	
	public static final String ITEMS_REMOVED = "ITEMS_REMOVED";
	
	private List<Item> items = new ArrayList<Item>();
	
	public void add(Item item) {
		items.add(item);
		if (item.getX() == -1) {
			call(ITEMS_EMPTY);
		} else {
			call(ITEMS_CHANGED);
		}
	}
	
	public void add(int x, int y, int z) {
		add(new Item(x, y, z));
	}

	public void add(int n) {
		if (n > 0) {
			while(n-- > 0) {
				items.add(new Item(-1, 0, 0));
				call(ITEMS_EMPTY);
			}
		}
	}
	
	public void del(int index) {
		if ((index >= 0) && (index < items.size())) {
			items.remove(index);
			call(ITEMS_REMOVED);
		}
	}
	
	public void del(Item item) {
		if (item != null) {
			items.remove(item);
			call(ITEMS_REMOVED);
		}
	}
	
	@Override
	public Iterator<Item> iterator() {
		return items.iterator();
	}

	public List<Item> getItems() {
		return items;
	}
}
