﻿package ua.khpi.project.task07;

import java.util.Collections;

/**
 * Наблюдатель, шаблон Observer.
 * @author davydova_milena
 *
 */
public class ItemsSorter extends AnnotatedObserver {
	
	public static final String ITEMS_SORTED = "ITEMS_SORTED";
	
	@Event(ITEMS_SORTED)
	public void itemSorted(ItemCollection observable) {
		System.out.println(observable.getItems());
	}
	
	@Event(ItemCollection.ITEMS_REMOVED) 
	public void itemsRemoved(ItemCollection observable) {
		System.out.println(observable.getItems());
	}
	
	@Event(ItemCollection.ITEMS_CHANGED)
	public void itemsChanged(ItemCollection observable) {
		Collections.sort(observable.getItems());
		observable.call(ITEMS_SORTED);
	}

}
