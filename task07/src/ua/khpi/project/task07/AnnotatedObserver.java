﻿package ua.khpi.project.task07;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Базовый класс наблюдателя.
 * Использует аннотации при определении методов.
 * @author davydova_milena
 *
 */
public abstract class AnnotatedObserver implements Observer {

	private Map<Object, Method> handlers = new HashMap<Object, Method>();
	
	public AnnotatedObserver() {
		for (Method m : this.getClass().getMethods()) {
			if (m.isAnnotationPresent(Event.class)) {
				handlers.put(m.getAnnotation(Event.class).value(), m);
			}
		}
	}
	
	public void handleEvent(Observable obs, Object event) {
		Method m = handlers.get(event);
		try {
			if (m != null) 
				m.invoke(this, obs);
		} catch (Exception err) {
			System.out.println(err);
		}
	}
}
