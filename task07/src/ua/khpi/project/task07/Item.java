﻿package ua.khpi.project.task07;

/**
 * Элемент коллекции.
 * @author davydova_milena
 *
 */
public class Item implements Comparable<Item>{
	
	private int x;
	private int y;
	private int z;
	
	public Item(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void SetX(int x) {
		this.x = x;
	}
	
	public int getX() {
		return x;
	}
	
	public void SetY(int y) {
		this.y = y;
	}
	
	public int getY() {
		return y;
	}
	
	public void SetZ(int z) {
		this.z = z;
	}
	
	public int getZ() {
		return z;
	}
	
	public String toString() {
		return "x = " + x + " y = " + y + " z = " + z;
	}

	@Override
	public int compareTo(Item o) {
		return Integer.compare(x, o.getX());
	}
}
