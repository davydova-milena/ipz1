﻿package ua.khpi.project.task07;

import java.util.HashSet;
import java.util.Set;

/**
 * Средства взаимодействия между наблюдателями и наблюдаемым.
 * @author davydova_milena
 *
 */
public abstract class Observable {
	
	private Set<Observer> observers = new HashSet<Observer>();
	
	public void addObserver (Observer observer) {
		observers.add(observer);
	}
	
	public void delObserver (Observer observer) {
		observers.remove(observer);
	}
	
	public void call(Object event) {
		for(Observer observer : observers) {
			observer.handleEvent(this, event);
		}
	}
}
