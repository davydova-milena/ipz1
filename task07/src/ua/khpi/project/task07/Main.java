﻿package ua.khpi.project.task07;

import ua.khpi.project.task05.ConsoleCommand;
import ua.khpi.project.task05.Menu;

/**
 * Содержит метод main().
 * @author davydova_milena
 *
 */
public class Main {

	abstract class ConsoleCmd implements ConsoleCommand {
		
		protected ItemCollection items;
		
		private String name;
		
		private char key;
		
		ConsoleCmd(ItemCollection items, String name, char key) {
			this.items = items;
			this.name = name;
			this.key = key;
		}
		
		public char retKey() {
			return key;
		}
		
		public String toString() {
			return name;
		}
	}
	
	public void run() {
		ItemCollection items = new ItemCollection();
		ItemsCreator creator = new ItemsCreator();
		ItemsSorter sort = new ItemsSorter();
		items.addObserver(creator);
		items.addObserver(sort);
		Menu menu = new Menu();
		menu.add(new ConsoleCmd(items, "1 - View", '1') {
			public void execute() {
				System.out.println(items.getItems());
			}
		});
		menu.add(new ConsoleCmd(items, "2 - Add new", '2') {
			public void execute() {
				items.add(-1, 0, 0);
			}
		});
		menu.add(new ConsoleCmd(items, "3 - Delete", '3') {
			public void execute() {
				items.del((int)Math.round(Math.random()
						     * (items.getItems().size() - 1)));
			}
		});
		menu.execute();
	}
	
	public static void main(String[] args) {
		new Main().run();
	}

}
