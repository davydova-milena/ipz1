﻿package ua.khpi.project.task07;

/**
 * Наблюдатель, шаблон Observer.
 * @author davydova_milena
 *
 */
public class ItemsCreator extends AnnotatedObserver {

	@Event(ItemCollection.ITEMS_EMPTY)
	public void itemsEmpty (ItemCollection observable) {
		int x;
		int y;
		int z;
		for (Item item : observable) {
			if (item.getX() == -1) {
				x = ((int) (Math.random() * 99) + 1);	
				y = ((int) (Math.random() * 99));	
				z = ((int) (Math.random() * 99));	
				item.SetX(x);
				item.SetY(y);
				item.SetZ(z);
			}
		}
	observable.call(ItemCollection.ITEMS_CHANGED);
	}
}
