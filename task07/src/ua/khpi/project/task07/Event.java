﻿package ua.khpi.project.task07;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Аннотация RUNTIME для назначения конкретных событий.
 * @author davydova_milena
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
	String value();
}
