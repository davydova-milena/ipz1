﻿package ua.khpi.project.task07;

/**
 * Содержит метод для связи "наблюдатель-наблюдаемый".
 * @author davydova_milena
 *
 */
public interface Observer {
	public void handleEvent(Observable obs, Object event);
}
