﻿package ua.khpi.project.task05;

import ua.khpi.project.task04.ViewTable;

/**
 * Реализация консольной комманды - восстановления сохраненных данных.
 * @author davydova_milena
 *
 */
public class RestoreConsoleCommand implements ConsoleCommand {

	private ViewTable view;
	
	public RestoreConsoleCommand(ViewTable view) {
		this.view = view;
	}
	
	public String toString() {
		return "4 - Load last saved";
	}
	
	@Override
	public void execute() {
		System.out.println("Restoring");
		try {
			view.restoreView();
		} catch (Exception e) {
			System.out.println("Error loading: " + e);
		}
		view.fullView();
	}

	@Override
	public char retKey() {
		return '4';
	}
}
