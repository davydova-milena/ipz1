﻿package ua.khpi.project.task05;

import ua.khpi.project.task04.ViewTable;

/**
 * Реализация консольной комманды - создания новых данных массива.
 * @author davydova_milena
 *
 */
public class CreateConsoleCommand implements ConsoleCommand {

	private ViewTable view;
	
	public CreateConsoleCommand(ViewTable view) {
		this.view = view;
	}
	
	@Override
	public char retKey() {
		return '1';
	}
	
	public String toString() {
		return "1 - New array";
	}

	@Override
	public void execute() {
		System.out.println("Generating.");
		view.initView(80);
		view.fullView();
	}
}
