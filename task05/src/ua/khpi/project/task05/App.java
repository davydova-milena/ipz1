﻿package ua.khpi.project.task05;

import ua.khpi.project.task04.ViewTable;
import ua.khpi.project.task04.ViewerTable;

/**
 * Формирование пунктов меню. 
 * Использует шаблон Singleton.
 * @author davydova_milena
 *
 */
public class App {
	
	private static App instance = new App();
	
	private App() {}
	
	public static App getInstance() {
		return instance;
	}
	
	private ViewTable view =  new ViewerTable().createView();
	
	private Menu menu = new Menu();
	
	public void run() {
		menu.add(new CreateConsoleCommand(view));
		menu.add(new ViewConsoleCommand(view));
		menu.add(new SaveConsoleCommand(view));
		menu.add(new RestoreConsoleCommand(view));
		menu.add(new CalculateConsoleCommand(view));
		menu.execute();
	}
}
