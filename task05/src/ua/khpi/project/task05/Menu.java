﻿package ua.khpi.project.task05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Реализация меню и выборки комманд.
 * @author davydova_milena
 *
 */
public class Menu implements Command {
	
	private List<ConsoleCommand> menu = new ArrayList<ConsoleCommand>();
	
	public void add(ConsoleCommand command) {
		menu.add(command);
	}
	
	public String toString() {
		String str = "Commands:\n";
		for(ConsoleCommand cc : menu) {
			str += cc + ", ";
		}
		return str += "6 - Exit: ";
	}

	@Override
	public void execute() {
		String operation = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
		
		menu : while(true) {
			do {
				System.out.print(this);
				try {
					operation = in.readLine();
				} catch (IOException err) {
					System.err.println("Error: " + err);
					System.exit(0);
				}
			} while (operation.length() != 1);
			if(operation.charAt(0) == '6') {
				System.out.println("Quiting.");
				break menu;
			}
			for (ConsoleCommand cc : menu) {
				if(operation.charAt(0) == cc.retKey()) {
					cc.execute();
					continue menu;
				}
			}
			System.out.println("Bad command!");
			continue menu;
		}
	}
}
