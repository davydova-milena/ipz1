﻿package ua.khpi.project.task05;

import ua.khpi.project.task04.ViewTable;

/**
 * Реализация консольной комманды - вывод данных на консоль.
 * @author davydova_milena
 *
 */
public class ViewConsoleCommand implements ConsoleCommand {
	
	private ViewTable view;
	
	public ViewConsoleCommand(ViewTable view) {
		this.view = view;
	}
	
	@Override
	public char retKey() {
		return '2';
	}
	
	public String toString() {
		return "2 - Show array";
	}

	@Override
	public void execute() {
		System.out.println("Shownig.");
		view.fullView();
	}
}
