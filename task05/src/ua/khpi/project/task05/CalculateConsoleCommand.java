﻿package ua.khpi.project.task05;

import ua.khpi.project.task04.ViewTable;

/**
 * Реализация консольной комманды - операции над массивом.
 * @author davydova_milena
 *
 */
public class CalculateConsoleCommand implements ConsoleCommand {

	private ViewTable view;
	
	public CalculateConsoleCommand(ViewTable view) {
		this.view = view;
	}
	
	public String toString() {
		return "5 - Do calculations";
	}
	
	@Override
	public void execute() {
		System.out.println("Calculating.");
		view.calculate();
		view.fullView();
	}

	@Override
	public char retKey() {
		return '5';
	}

}
