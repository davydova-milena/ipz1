﻿package ua.khpi.project.task05;

/**
 * Интерфейс, описывающий консольную комманду.
 * @author davydova_milena
 *
 */
public interface ConsoleCommand extends Command {
	public char retKey();
}
