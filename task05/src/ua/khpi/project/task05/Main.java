﻿package ua.khpi.project.task05;

/**
 * Содержит main()
 * @author davydova_milena
 * @version 1.0.0
 */
public class Main {
	
	/** 
	 * Выполняется при запуске программы. 
	 * @param args Аргументы при запуске программы.
	 */  
	public static void main(String[] args) {
		App app = App.getInstance();
		app.run();
	}
}

