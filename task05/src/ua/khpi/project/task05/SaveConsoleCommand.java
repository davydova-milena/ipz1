﻿package ua.khpi.project.task05;

import ua.khpi.project.task04.ViewTable;

/**
 * Реализация консольной комманды - сохранения данных.
 * @author davydova_milena
 *
 */
public class SaveConsoleCommand implements ConsoleCommand {
	
	private ViewTable view;
	
	public SaveConsoleCommand(ViewTable view) {
		this.view = view;
	}
	
	public String toString() {
		return "3 - Save current array";
	}
	
	@Override
	public void execute() {
		System.out.println("Saving");
		try {
			view.saveView();
		} catch (Exception e) {
			System.out.println("Error saving: " + e);
		}
	}

	@Override
	public char retKey() {
		return '3';
	}
}
