package ua.khpi.project.task05;

/**
 * Интерфейс комманды.
 * @author davydova_milena
 *
 */
public interface Command {
	public void execute();
}
