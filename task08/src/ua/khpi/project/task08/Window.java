﻿package ua.khpi.project.task08;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import ua.khpi.project.task03.Dimensions;
import ua.khpi.project.task03.View;

/**
 * Создание окна с графиком.
 * @author davydova_milena
 *
 */
@SuppressWarnings("serial")
public class Window extends Frame{
	
	private static final int BORDER = 20;
	
	private View view;
	
	public Window(View view) {
		this.view = view;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				setVisible(false);
			}
		});
	}
	
	public void paint(Graphics g) {
		Rectangle r = getBounds(), c = new Rectangle();
		r.x += BORDER;
		r.y += 35 + BORDER;
		r.width -= r.x + BORDER;
		r.height -= r.y + BORDER;
		c.x = r.x;
		c.y = r.y + r.height / 2;
		g.setColor(Color.CYAN);
		g.setColor(Color.RED);
		g.drawLine(c.x, c.y, c.x + c.width, c.y);
		g.drawLine(c.x, r.y, c.x, r.y + r.height);
		double x = 0, y;
		y = view.calcBinary(view.getList().get(0).getHeight());
		for(Dimensions obj : view.getList()) {
			if (view.calcBinary(obj.getWidth()) > x) {
				x = view.calcBinary(obj.getWidth());			
				}
			if(view.calcBinary(obj.getHeight()) > y) {
				y = view.calcBinary(obj.getHeight());
			}
		}
		g.drawString("+" + y, r.x, r.y);
		g.drawString("-" + y, r.x, r.y + r.height);
		g.drawString("+" + x, c.x + c.width -
				g.getFontMetrics().stringWidth("+" + x), c.y);

		g.setColor(Color.DARK_GRAY);
		for (Dimensions obj : view.getList()) {
			g.drawOval(c.x + (int) (view.calcBinary(obj.getWidth())) - 5,
					c.y - (int) (view.calcBinary(obj.getHeight())) - 5, 5, 5);
		}
	}
}
