﻿package ua.khpi.project.task08;

import ua.khpi.project.task03.BaseViewer;
import ua.khpi.project.task03.View;

/**
 * Реализация метода для фабрикации объектов.
 * @author davydova_milena
 *
 */
public class ViewerWindow implements BaseViewer {

	@Override
	public View createView() {
		return new ViewWindow();
	}

}
