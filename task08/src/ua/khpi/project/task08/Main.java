﻿package ua.khpi.project.task08;

/**
 * Содержит метод main().
 * @author davydova_milena
 *
 */
public class Main{
	
	public static void main(String[] args) {
		Applet app = Applet.getInstance();
		
		app.run(new ViewerWindow().createView());
	}
}
