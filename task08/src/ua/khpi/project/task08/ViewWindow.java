﻿package ua.khpi.project.task08;

import java.awt.Dimension;

import ua.khpi.project.task03.View;

/**
 * "Фабрикуемый объект". 
 * Отображает график.
 * @author davydova_milena
 *
 */
public class ViewWindow extends View {
	
	private static final int NUM_ELEM = 10;
	
	private Window window = null;
	
	public ViewWindow() {
		super(NUM_ELEM);
		window = new Window(this);
		window.setSize(new Dimension(640, 480));
		window.setTitle("Result");
		window.setVisible(true);
	}
	
	public void initView() {
		super.initView();
	}
	
	public void fullView() {
		super.fullView();
		window.setVisible(true);
		window.repaint();
	}
}
