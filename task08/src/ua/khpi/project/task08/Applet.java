package ua.khpi.project.task08;

import ua.khpi.project.task03.View;
import ua.khpi.project.task05.CalculateConsoleCommand;
import ua.khpi.project.task05.CreateConsoleCommand;
import ua.khpi.project.task05.RestoreConsoleCommand;
import ua.khpi.project.task05.SaveConsoleCommand;
import ua.khpi.project.task05.ViewConsoleCommand;

public class Applet extends ua.khpi.project.task05.App {
	
	protected static Applet instance = new Applet();
	
	public Applet() {}
	
	public static Applet getInstance() {
		return instance;
	}

	public void run(View view) {
		menu.add(new CreateConsoleCommand(view));
		menu.add(new ViewConsoleCommand(view));
		menu.add(new SaveConsoleCommand(view));
		menu.add(new RestoreConsoleCommand(view));
		menu.add(new CalculateConsoleCommand(view));
		menu.execute();
	}
}
