﻿package ua.khpi.project.task06;

import ua.khpi.project.task05.Command;

/**
 * Интерфейс очереди.
 * @author davydova_milena
 *
 */
public interface Queue {

	void put(Command cmd);
	Command take();
}
