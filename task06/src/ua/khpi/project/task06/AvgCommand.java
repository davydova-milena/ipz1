package ua.khpi.project.task06;

import ua.khpi.project.task05.Command;

import java.util.concurrent.TimeUnit;

import ua.khpi.project.task03.Dimensions;
import ua.khpi.project.task03.View;

/**
 * 
 * @author davydova_milena
 *
 */
public class AvgCommand implements Command {

	private double res = .0;
	
	private int progress = 0;
	
	private View view;
	
	public View getView() {
		return view;
	}
	
	public void setView(View view) {
		this.view = view;
	}
	
	public AvgCommand(View view) {
		this.view = view;
	}
	
	public double getRes() {
		return res;
	}
	
	public boolean status() {
		return progress < 100;
	}
	
	@Override
	public void execute() {
		progress = 0;
		System.out.println("Finding avg by P.");
		res = .0;
		int idx = 1;
		int size = view.getList().size();
		for(Dimensions obj : view.getList()) {
			res += obj.getPerimeter();
			progress = idx * 100 / size;
			if(idx++ % (size / 2) == 0) {
				System.out.println("Avg: " + progress + "%");
			}
			try {
				TimeUnit.MILLISECONDS.sleep(2000 / size);
			} catch (InterruptedException err) {
				System.out.println("Error: " + err);
			}
		}
		res /= size;
		System.out.println("Average - " + String.format("%.3f", res));
		progress = 100;
	}
}
