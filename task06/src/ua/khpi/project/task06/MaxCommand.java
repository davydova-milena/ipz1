package ua.khpi.project.task06;

import java.util.concurrent.TimeUnit;

import ua.khpi.project.task03.View;
import ua.khpi.project.task05.Command;

/**
 * 
 * @author davydova_milena
 *
 */
public class MaxCommand implements Command {
	
	private int res = 0;
	
	private int progress = 0;
	
	private View view;
	
	public View getView() {
		return view;
	}
	
	public void setView(View view) {
		this.view = view;
	}
	
	public MaxCommand(View view) {
		this.view = view;
	}
	
	public double getRes() {
		return res;
	}
	
	public boolean status() {
		return progress < 100;
	}
	
	@Override
	public void execute() {
		progress = 0;
		System.out.println("Finding max P.");
		res = 0;
		int size = view.getList().size();
		for(int idx = 1; idx < size; idx++) {
			if(view.getList().get(res).getPerimeter() < 
					view.getList().get(idx).getPerimeter()) {
				res = idx;
			}
			progress = idx * 100 / size;
			if(idx % (size / 2) == 0) {
				System.out.println("Max: " + progress + "%");
			}
			try {
				TimeUnit.MILLISECONDS.sleep(2000 / size);
			} catch (InterruptedException err) {
				System.out.println("Error: " + err);
			}
		}
		System.out.println("Elem num - " + (res + 1) + ".");
		System.out.println("Max - " + String.format("%.3f", view.getList().get(res).getPerimeter()));
		progress = 100;
	}
}
