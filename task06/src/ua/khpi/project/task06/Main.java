package ua.khpi.project.task06;

import ua.khpi.project.task03.View;
import ua.khpi.project.task03.Viewer;
import ua.khpi.project.task05.CalculateConsoleCommand;
import ua.khpi.project.task05.CreateConsoleCommand;
import ua.khpi.project.task05.Menu;
import ua.khpi.project.task05.ViewConsoleCommand;

/**
 * 
 * @author davydova_milena
 *
 */
public class Main {

	private View view = (View) new Viewer().createView();
	
	private Menu menu = new Menu();
	
	public void run() {
		menu.add(new CreateConsoleCommand(view));
		menu.add(new ViewConsoleCommand(view));
		menu.add(new CalculateConsoleCommand(view));
		menu.add(new ExecConsoleCommand(view));
		menu.execute();
	}
	
	public static void main(String[] args) {
		Main main = new Main();
		main.run();
	}

}
