package ua.khpi.project.task06;

import java.util.concurrent.TimeUnit;

import ua.khpi.project.task03.View;
import ua.khpi.project.task05.ConsoleCommand;

/**
 * 
 * @author davydova_milena
 *
 */
public class ExecConsoleCommand implements ConsoleCommand {

	private View view;
	
	public View getView() {
		return view;
	}
	
	public void setView(View view) {
		this.view = view;
	}
	
	public ExecConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public void execute() {
		CommandQueue q1 = new CommandQueue();
		CommandQueue q2 = new CommandQueue();
		MaxCommand maxCmd = new MaxCommand(view);
		AvgCommand avgCmd = new AvgCommand(view);
		FindCommand findCmd = new FindCommand(view);
		System.out.println("Begin all threads.");
		q1.put(avgCmd);
		q1.put(findCmd);
		q2.put(maxCmd);
		try {
			while (avgCmd.status() ||
					maxCmd.status() ||
					findCmd.status()) {
				TimeUnit.MILLISECONDS.sleep(100);
			}
			q1.shutdown();
			q2.shutdown();
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException err) {
			System.err.println("Error: " + err);
		}
		System.out.println("Done.");
	}
	
	public String toString() {
		return "0 - Do all";
	}

	@Override
	public char retKey() {
		return '0';
	}

}
