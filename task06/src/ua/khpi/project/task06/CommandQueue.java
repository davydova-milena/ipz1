﻿package ua.khpi.project.task06;

import java.util.Vector;
import ua.khpi.project.task05.Command;

/**
 * Реализация очереди задач.
 * @author davydova_milena
 *
 */
public class CommandQueue implements Queue {

	private Vector<Command> tasklist;
	
	private boolean waiting;
	
	private boolean shutdown;
	
	public void shutdown() {
		shutdown = true;
	}
	
	public CommandQueue() {
		tasklist = new Vector<Command>();
		waiting = false;
		new Thread(new Worker()).start();
	}
	
	@Override
	public void put(Command cmd) {
		tasklist.add(cmd);
		if(waiting) {
			synchronized(this) {
				notifyAll();
			}
		}
		
	}

	@Override
	public Command take() {
		if(tasklist.isEmpty()) {
			synchronized(this) {
				waiting = true;
				try {
					wait();
				} catch (InterruptedException err) {
					waiting = false;
				}
			}
		}
		return (Command) tasklist.remove(0);
	}
	
	private class Worker implements Runnable {
	
		public void run() {
			while(!shutdown) {
				Command cmd = take();
				cmd.execute();
			}
		}
	}
}

