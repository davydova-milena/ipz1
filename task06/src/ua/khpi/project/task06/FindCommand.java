package ua.khpi.project.task06;

import java.util.concurrent.TimeUnit;

import ua.khpi.project.task03.View;
import ua.khpi.project.task05.Command;

/**
 * 
 * @author davydova_milena
 *
 */
public class FindCommand implements Command {
	
	private int res = -1;
	
	private int progress = 0;
	
	private View view;
	
	public View getView() {
		return view;
	}
	
	public void setView(View view) {
		this.view = view;
	}
	
	public FindCommand(View view) {
		this.view = view;
	}
	
	public double getRes() {
		return res;
	}
	
	public boolean status() {
		return progress < 100;
	}
	
	@Override
	public void execute() {
		progress = 0;
		int rand = 51 + (int) (Math.random() * 200);
		System.out.println("Finding elem with P = " + rand + " (+-50).");
		res = -1;
		int idx = 1;
		int size = view.getList().size();
		for(int i = 0; i < size; i++) {
			if((rand-50) <= view.getList().get(i).getPerimeter() 
					|| view.getList().get(i).getPerimeter() <= (rand+50)) {
				res = i;
			}
			progress = idx * 100 / size;
			if(idx++ % (size / 2) == 0) {
				System.out.println("Find: " + progress + "%");
			}
			try {
				TimeUnit.MILLISECONDS.sleep(2000 / size);
			} catch (InterruptedException err) {
				System.out.println("Error: " + err);
			}
		}
		if(res != -1) {
			System.out.println("Elem num - " + (res + 1) + ".");
			System.out.println("Found P - " + String.format("%.3f", view.getList().get(res).getPerimeter()));
		} else {
			System.out.println("No elem found.");
		}
		progress = 100;
	}
}
