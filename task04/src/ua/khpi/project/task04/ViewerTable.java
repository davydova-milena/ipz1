﻿package ua.khpi.project.task04;

import ua.khpi.project.task03.BaseView;
import ua.khpi.project.task03.Viewer;

/**
 * Класс с методом для "фабрикации" объектов.
 * @author davydova_milena
 *
 */
public class ViewerTable extends Viewer {
	
	public BaseView createView() {
		return new ViewTable();
	}
}
