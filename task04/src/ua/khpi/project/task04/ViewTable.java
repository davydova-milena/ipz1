﻿package ua.khpi.project.task04;

import java.util.Scanner;
import java.util.Formatter;

import ua.khpi.project.task03.View; 
import ua.khpi.project.task03.Dimensions; 

/**
 * Класс для расчета и вывода результатов ввиде таблицы. 
 * @author davydova_milena
 * @version 1.0.0
 */
public class ViewTable extends View {
	
	private static final int BASIC_WIDTH = 80;
	
	private int width;

	public ViewTable() {
		setWidth(BASIC_WIDTH);
	}
	
	public ViewTable(int width) {
		this.setWidth(width);
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	
	private void separator() {
		for(int i = 0; i < width; i++) {
			System.out.print('-');
		}
		System.out.println();
	}
	
	private void outHead() {
		separator();
		Formatter form = new Formatter();
		form.format("%s%d%s%2$d%3$s%2$d%3$s%2$d%3$s%2$d%3$s%2$d%4$s", "%", -((width/9)+2), "s | %", "s%n");
		System.out.printf(form.toString(), "  height", "  width", "  length", "    P", "    A ", "    V ");
		separator();
		form.close();
	}

	private void outContent() {
		Formatter form = new Formatter();
		form.format("%s%d%s%2$d%3$s%2$d%3$s%2$d%5$s%2$d%5$s%2$d%4$s", "%", -((width/9)+2), "s | %", ".3f%n", ".3f | %");
		for(Dimensions obj: list) {
			System.out.printf(form.toString(), obj.getHeight(), obj.getWidth(), obj.getLength(), obj.getPerimeter(), obj.getArea(), obj.getVolume());			
		}
		form.close();
	}
	
	public void showHeader() {
		outHead();
	}
	
	public void showContent() {
		outContent();
	}
	
	public void showFooter() {
		separator();
	}
	
	public void fullView() {
		showHeader();
		showContent();
		showFooter();
	}
	
	public void initView(int width) {
		this.width = width;
		super.initView();
	}
	
	public void initView() {
		Scanner reader = new Scanner(System.in);
		int width = 0;
		System.out.println("Initializing!");
 		System.out.println("Input the new width (0 - left default): ");    
		width = reader.nextInt();   
		if(width == 0) {
			super.initView();
		} else {
			if(width < BASIC_WIDTH) {
				System.out.println("Bad width! Creating table with basic width.");
				super.initView();
			} else {
				initView(width);
			}
		}
	}
}