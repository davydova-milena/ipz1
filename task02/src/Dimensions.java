﻿package ua.khpi.project.task02;

import java.io.Serializable;

/**
 * Содержит значения и результат их вычислений.
 * @author davydova_milena
 * @version 1.0.0
 */
public class Dimensions implements Serializable {
	/** Автоматически сгенерированная константа */
	private static final long serialVersionUID = 1L;
	/** Параметр высоты. */
	private String height;
	/** Параметр длины. */
	private String length;
	/**	Параметр ширины. */
	private String width;
	/** Расчетный параметр периметра. */
	private transient double perimeter;	
	/** Расчетный параметр площади. */
	private transient double area;
	/** Расчетный параметр объема. */
	private transient double volume;
	/**
	 * Конструктор класса {@linkplain Dimensions}}
	 */
	public Dimensions() {
		height = "0";
		length = "0";
		width = "0";
		perimeter = .0;
		area = .0;
		volume = .0;
	}	
	/**
	 * Конструктор с параметрами класса {@linkplain Dimensions}}
	 */
	public Dimensions(String height, String length, String width, double perimeter,
					  double area, double volume) {
		this.height = height;
		this.length = length;
		this.width = width;
		this.perimeter = perimeter;
		this.area = area;
		this.volume = volume;
	}
	/**
	 * Получение значения поля {@linkplain Dimensions#height} 
	 * @return Значение {@linkplain Dimensions#height} 
	 */
	public String getHeight() {
		return height;
	}
	/**
	 * Установка значения поля {@linkplain Dimensions#height} 
	 * @param height Значение {@linkplain Dimensions#height} 
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * Получение значения поля {@linkplain Dimensions#length} 
	 * @return Значение {@linkplain Dimensions#length} 
	 */
	public String getLength() {
		return length;
	}
	/**
	 * Установка значения поля {@linkplain Dimensions#length} 
	 * @param length Значение {@linkplain Dimensions#length} 
	 */
	public void setLength(String length) {
		this.length = length;
	}
	/**
	 * Получение значения поля {@linkplain Dimensions#width} 
	 * @return Значение {@linkplain Dimensions#width}
	 */
	public String getWidth() {
		return width;
	}
	/**
	 * Установка значения поля {@linkplain Dimensions#width} 
	 * @param width Значение {@linkplain Dimensions#width} 
	 */
	public void setWidth(String width) {
		this.width = width;
	}
	/**
	 * Получение значения поля {@linkplain Dimensions#perimeter} 
	 * @return Значение {@linkplain Dimensions#perimeter} 
	 */
	public double getPerimeter() {
		return perimeter;
	}
	/**
	 * Установка значения поля {@linkplain Dimensions#perimeter} 
	 * @param perimeter Значение {@linkplain Dimensions#perimeter} 
	 */
	public void setPerimeter(double perimeter) {
		this.perimeter = perimeter;
	}
	/**
	 * Получение значения поля {@linkplain Dimensions#area} 
	 * @return Значение {@linkplain Dimensions#area} 
	 */
	public double getArea() {
		return area;
	}
	/**
	 * Установка значения поля {@linkplain Dimensions#area}
	 * @param area Значение {@linkplain Dimensions#area} 
	 */
	public void setArea(double area) {
		this.area = area;
	}
	/**
	 * Получение значения поля {@linkplain Dimensions#volume} 
	 * @return Значение {@linkplain Dimensions#volume} 
	 */
	public double getVolume() {
		return volume;
	}
	/**
	 * Установка значения поля {@linkplain Dimensions#volume} 
	 * @param volume Значение {@linkplain Dimensions#volume} 
	 */
	public void setVolume(double volume) {
		this.volume = volume;
	}
	/**
	 * Установка значений расчетных полей {@linkplain Dimensions#perimeter},
	 * {@linkplain Dimensions#area}, {@linkplain Dimensions#volume}
	 * @param perimeter Значение {@linkplain Dimensions#perimeter} ++
	 * @param area Значение {@linkplain Dimensions#area} 
	 * @param volume Значение {@linkplain Dimensions#volume} 
	 */
	public void setCalc(double perimeter, double area, double volume) {
		this.perimeter = perimeter;
		this.area = area;
		this.volume = volume;
	}
	/**
	 * Представляет параметры объекта в виде строки.
	 */
	@Override
	public String toString() {
		return "height = " + height + " width = " + width + " length = " + length + "\n"
				+ "Perimeter = " + perimeter + " Area = " + area + " Volume = " + volume;
	}
}
