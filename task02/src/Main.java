﻿package ua.khpi.project.task02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Класс, содержащий меню и main().
 * @author davydova_milena
 * @version 1.0.0
 */
public class Main {
	/** Максимально допустимое кол-во символов. */
	private static int MAX_BINARY = 8;
	/** Объект класса {@linkplain Calc} */
	private Calc calc = new Calc();
	/** Объект класса {@linkplain Dimensions} */
	private Dimensions dim = new Dimensions();
	/**
	 * Метод для создания рандомизированого числа в бинорном виде.
	 * @return Бинарный вид числа.
	 */
	private String randBin() {
		StringBuilder sb = new StringBuilder(MAX_BINARY);
		for(int i = 0; i < MAX_BINARY; i++) {
		     sb.append((int) (Math.random() + 0.5));
		}
		if(((int) (Math.random() + 0.5)) == 0) {
			sb.setCharAt((1 + (int) (Math.random() * (MAX_BINARY - 2))), '.');
		}
		return sb.toString();
	}
	/**
	 * Вывод меню.
	 */
	private void menu() {
		String operation = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
		System.out.println("Starting.");
		System.out.println("1 - New obj | 2 - View | 3 - Save | 4 - Restore | 5 - Calculate | 6 - Quit");
		do {
			 try {      
				 operation = in.readLine();     
				 } catch(IOException err) {      
					 System.out.println("Error: " + err);
					 System.exit(0);     
			 }
			 switch(operation.charAt(0)) {
			 	case '1': {
			 		System.out.println("Creating new random obj.");
			 		dim.setWidth(randBin());
			 		dim.setHeight(randBin());
			 		dim.setLength(randBin());
			 		calc.setObj(dim);
			 		calc.show();
			 		break;
			 	}
			 	
			 	case '2': {
			 		System.out.println("Showing current obj.");
			 		calc.show();
			 		break;
			 	}
			 	
			 	case '3': {
			 		System.out.println("Saving.");
			 		try {      
			 			calc.save();
			 			} catch (IOException err) {      
			 				System.out.println("Error saving: " + err);     
			 		}         
			 		break;   
			 	}
			 	
			 	case '4': {
			 		System.out.println("Restoring.");
			 		try {      
			 			calc.restore();
			 			} catch (Exception err) { 
			 			     System.out.println("Error restoring: " + err); 
			 			}     
			 		calc.show(); 
			 		break;
			 	}	
			 	
			 	case '5': {
			 		System.out.println("Calculating.");
			 		calc.calcDimension();
			 		break;
			 	}	
			 	
			 	case '6': {
			 		System.out.println("Shutting down.");
			 		break;
			 	}
			 	
			 	default: {
			 		System.out.println("Wrong operation.");
			 	}
			 }			
		} while (operation.charAt(0) != '6');
	}
	/** 
	 * Выполняется при запуске программы. 
	 * @param args Аргументы при запуске программы.
	 */  
	public static void main(String[] args) {
		Main main = new Main();
		main.menu();
	}
}
