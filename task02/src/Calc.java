﻿package ua.khpi.project.task02;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream; 
import java.io.ObjectOutputStream; 

/**
 * Класс для расчета и вывода результатов. 
 * @author davydova_milena
 * @version 1.0.0
 */
public class Calc {
	/** Имя файла при сериализации. */
	private static final String FNAME = "dimensions.bin"; 
	
	/** Объект для выполнения операций. */
	private Dimensions obj;
	
	/**
	 * Конструктор без параметров.
	 */
	public Calc() {
		setObj(new Dimensions());
	}

	/**
	 * Получение значения поля {@linkplain Calc#obj} 
	 * @return Значение {@linkplain Calc#obj} 
	 */
	public Dimensions getObj() {
		return obj;
	}

	/**
	 * Установка значения поля {@linkplain Calc#obj}
	 * @param obj Значение {@linkplain Calc#obj} 
	 */
	public void setObj(Dimensions obj) {
		this.obj = obj;
	}
		
	/**
	 * Метод для преобразования из бинарного вида в десятичное число.
	 * @param str Бинарное представление числа
	 * @return Значение параметра в десятичном виде.
	 */
	public double calcBinary(String str) {
		double intgr = 0;
		double fract = 0;
		double beforeFract = 0;
		double afterFract;	
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i) == '.') {
				break;
			}
			beforeFract++;
		}
		afterFract =  (str.length() - beforeFract - 2);
		beforeFract = Math.pow(2, (beforeFract - 1));
		afterFract = Math.pow(2, afterFract);	
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i) == '.') {
				for(int k = (i + 1); k < str.length(); k++) {
					if(str.charAt(k) == '1') {
						fract += afterFract;
						afterFract /= 2;
					} else {
						afterFract /= 2;
					}
				}
			fract *= Math.pow(10, -(str.length() - i + 1));
			break;
			} else {
				if(str.charAt(i) == '1') {
					intgr += beforeFract;
					beforeFract /= 2;
				} else {
					beforeFract /= 2;
				}
			}
		}
	return intgr + fract;
	}
	/**
	 * Расчет периметра, площади и объема заданого объекта {@linkplain Calc#obj}.
	 */
	public void calcDimension() {
		double len = calcBinary(obj.getLength());
		double wid = calcBinary(obj.getWidth());
		double height = calcBinary(obj.getHeight());
		
		obj.setPerimeter(2 * (len + wid));
		obj.setArea(len * wid);
		obj.setVolume(len * wid * height);
	}
	/** 
	 * Сохраняет {@linkplain Calc#obj} в файле {@linkplain Calc#FNAME}   
	 * @throws IOException   
	 */
	public void save() throws IOException {   
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(obj);
		os.flush();
		os.close();
	}
	/** 
	 * Восстанавливает {@linkplain Calc#obj} из файла {@linkplain Calc#FNAME}   
	 * @throws Exception   
	 */ 
	public void restore() throws Exception {   
		 ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		 obj = (Dimensions)is.readObject();
		 is.close();
	}
	/**
	 * Выводит на консоль {@linkplain Calc#obj}}
	 */
	public void show() {
		 System.out.println(obj);
	} 
}
