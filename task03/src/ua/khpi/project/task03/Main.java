﻿package ua.khpi.project.task03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * @author davydova_milena
 * @version 1.0.0
 */
public class Main {

	protected BaseView view;
	
	public Main(BaseView view) {
		this.view = view;
	}
	
	/**
	 * Вывод меню.
	 */
	protected void menu() {
		String operation = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
		System.out.println("Starting.");
		System.out.println("1 - New array | 2 - View | 3 - Save | 4 - Restore | 5 - Calculate | 6 - Quit");
		do {
			 try {      
				 operation = in.readLine();     
				 } catch(IOException err) {      
					 System.out.println("Error: " + err);
					 System.exit(0);     
			 }
			 switch(operation.charAt(0)) {
			 	case '1': {
			 		System.out.println("Creating new array.");
			 		view.initView();
			 		break;
			 	}
			 	
			 	case '2': {
			 		System.out.println("Showing array.");
			 		view.fullView();
			 		break;
			 	}
			 	
			 	case '3': {
			 		System.out.println("Saving.");
			 		try {      
			 			view.saveView();;
			 			} catch (IOException err) {      
			 				System.out.println("Error saving: " + err);     
			 		}         
			 		break;   
			 	}
			 	
			 	case '4': {
			 		System.out.println("Restoring.");
			 		try {      
			 			view.restoreView();
			 			} catch (Exception err) { 
			 			     System.out.println("Error restoring: " + err); 
			 			}     
			 		view.fullView(); 
			 		break;
			 	}	
			 	
			 	case '5': {
			 		System.out.println("Calculating.");
			 		view.calculate();
			 		break;
			 	}	
			 	
			 	case '6': {
			 		System.out.println("Shutting down.");
			 		break;
			 	}
			 	
			 	default: {
			 		System.out.println("Wrong operation.");
			 	}
			 }			
		} while (operation.charAt(0) != '6');
	}
	
	/** 
	 * Выполняется при запуске программы. 
	 * @param args Аргументы при запуске программы.
	 */  
	public static void main(String[] args) {
		Main main = new Main(new Viewer().createView());
		main.menu();
	}
}

