package ua.khpi.project.task03;

import java.io.IOException;

public interface BaseView {
	
	public void fullView();
	
	public void showHeader();
	
	public void showContent();

	public void showFooter();
	
	public void initView();
	
	public void saveView() throws IOException;

	public void restoreView() throws Exception;
	
	public void calculate();
	
}
