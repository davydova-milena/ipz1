﻿package ua.khpi.project.task03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Класс для расчета и вывода результатов.
 * @author davydova_milena
 * @version 1.0.0
 */
public class View implements BaseView {
	/** Максимально допустимое кол-во символов. */
	private static int MAX_BINARY = 8;
	
	/** Имя файла при сериализации */
	private static final String FNAME = "dimensions.bin";
	
	/** Объекты для выполнения операций. */
	protected ArrayList<Dimensions> list = new ArrayList<Dimensions>();
	
	private static final int AMOUNT = 5;
	
	/**
	 * Конструктор без параметров.
	 */
	public View() {
		this(AMOUNT);
	}

	public View(int num) {
		for (int i = 0; i < num; i++) {
			list.add(new Dimensions());
		}
	}
	/**
	 * Получение значения поля {@linkplain View#obj}.
	 * @return Значение {@linkplain View#obj}
	 */
	public ArrayList<Dimensions> getList() {
		return list;
	}
	/**
	 * Метод для преобразования из бинарного вида в десятичное число.
	 * @param str Бинарное представление числа
	 * @return Значение параметра в десятичном виде.
	 */
	public double calcBinary(String str) {
		double intgr = 0;
		double fract = 0;
		double beforeFract = 0;
		double afterFract;
		
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i) == '.') {
				break;
			}
			beforeFract++;
		}
		
		afterFract =  (str.length() - beforeFract - 2);
		beforeFract = Math.pow(2, (beforeFract - 1));
		afterFract = Math.pow(2, afterFract);
		
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i) == '.') {
				for(int k = (i + 1); k < str.length(); k++) {
					if(str.charAt(k) == '1') {
						fract += afterFract;
						afterFract /= 2;
					} else {
						afterFract /= 2;
					}
				}
			fract *= Math.pow(10, -(str.length() - i + 1));
			break;
			} else {
				if(str.charAt(i) == '1') {
					intgr += beforeFract;
					beforeFract /= 2;
				} else {
					beforeFract /= 2;
				}
			}
		}
	return intgr + fract;
	}
	/**
	 * Расчет периметра, площади и объема заданого
	 * объекта {@linkplain View#obj}.
	 */
	public void calculate() {
		for(Dimensions obj: list) {
			double len = calcBinary(obj.getLength());
			double wid = calcBinary(obj.getWidth());
			double height = calcBinary(obj.getHeight());
			obj.setPerimeter(2 * (len + wid));
			obj.setArea(len * wid);
			obj.setVolume(len * wid * height);
		}
	}
	/** 
	 * Сохраняет {@linkplain View#obj} в файле {@linkplain View#FNAME}   
	 * @throws IOException   
	 */
	public void saveView() throws IOException {   
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(list);
		os.flush();
		os.close();
	}
	/** 
	 * Восстанавливает {@linkplain View#obj} из файла {@linkplain View#FNAME}   
	 * @throws Exception   
	 */ 
	@SuppressWarnings("unchecked")
	public void restoreView() throws Exception {
		 ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		 list = (ArrayList<Dimensions>) is.readObject();
		 is.close();
	}

	@Override
	public void showHeader() {
		System.out.println("Array: ");
	}

	@Override
	public void showContent() {
	for(Dimensions obj: list) {
		System.out.println(obj.toString());
		}
	}

	@Override
	public void showFooter() {
		System.out.println("End.");
	}

	@Override
	public void fullView() {
		showHeader();
		showContent();
		showFooter();
	}

	@Override
	public void initView() {
		for(Dimensions obj: list) {
			obj.setHeight(randBin());
			obj.setLength(randBin());
			obj.setWidth(randBin());
		}
	} 

	public String randBin() {
		StringBuilder sb = new StringBuilder(MAX_BINARY);
		for(int i = 0; i < MAX_BINARY; i++) {
		     sb.append((int) (Math.random() + 0.5));
		}
		
		if(((int) (Math.random() + 0.5)) == 0) {
			sb.setCharAt((1 + (int) (Math.random() * (MAX_BINARY - 2))), '.');
		}
		return sb.toString();
	}
}
